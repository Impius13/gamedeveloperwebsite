<?php
    require_once "include/header.php";
?>

<div id="content">
    <div class="block container col-lg-7 text-center">
        <h1>Just a bunch of video game enthusiasts</h1>
        <div class="mt-3">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ut purus lacinia, placerat nisi vitae, luctus purus. Morbi ultrices interdum dolor. Donec molestie purus magna, id accumsan magna luctus ac. Nunc libero arcu, faucibus in maximus sed, iaculis vel dolor. Integer dui eros, semper vel augue at, consectetur gravida augue. Aenean et consequat lorem, et tristique tortor. Cras auctor venenatis purus non convallis. </p>
            <p>Proin imperdiet imperdiet lectus et pulvinar. Mauris cursus faucibus accumsan. Aenean feugiat massa id porta tempor. Fusce sagittis urna quis egestas feugiat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus viverra quis ante id accumsan. Ut porttitor arcu eu ex dictum, eu dictum dolor porttitor. Proin cursus dapibus mi, at fermentum libero rutrum sit amet. Morbi vel justo consequat, pulvinar tortor sit amet, interdum est. Ut non felis a quam egestas vehicula. </p>
        </div>
    </div>
    <div class="block container col-lg-7">
        <h2>From simple Android game to RPG's</h2>
        <div class="clearfix">
            <div class="mt-5">
                <img src="img/fallStuff2.jpg" class="games-screen float-lg-start me-4" alt="Fall Stuff Screenshot">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ut purus lacinia, placerat nisi vitae, luctus purus. Morbi ultrices interdum dolor. Donec molestie purus magna, id accumsan magna luctus ac. Nunc libero arcu, faucibus in maximus sed, iaculis vel dolor. Integer dui eros, semper vel augue at, consectetur gravida augue. Aenean et consequat lorem, et tristique tortor. Cras auctor venenatis purus non convallis. </p>
                <p>Proin imperdiet imperdiet lectus et pulvinar. Mauris cursus faucibus accumsan. Aenean feugiat massa id porta tempor. Fusce sagittis urna quis egestas feugiat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus viverra quis ante id accumsan. Ut porttitor arcu eu ex dictum, eu dictum dolor porttitor. Proin cursus dapibus mi, at fermentum libero rutrum sit amet. Morbi vel justo consequat, pulvinar tortor sit amet, interdum est. Ut non felis a quam egestas vehicula. </p>
            </div>
        </div>
    </div>
    <div class="block container col-lg-7 mt-5">
        <h3>Contact</h3>
        <p>Write us something nice!</p>
        <form action="backend/contact.php" method="post">
            <?php
                $name = "";
                $surname = "";
                $email = "";
                $subject = "";
                $message = "";
                // If user left some fields empty
                if (isset($_GET['error']) && $_GET['error'] == "emptyfields") {
                    $name = $_GET['name'];
                    $surname = $_GET['surname'];
                    $email = $_GET['email'];
                    $subject = $_GET['subject'];
                    $message = $_GET['message'];
                }
            ?>
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="contactName">Name:</label>
                    <input type="text" name="name" class="form-control" id="contactName" placeholder="John" value="<?php echo $name; ?>" required>
                </div>
                <div class="form-group col-md-6">
                    <label for="contactSurname">Surname:</label>
                    <input type="text" name="surname" class="form-control" id="contactSurname" placeholder="Doe" value="<?php echo $surname; ?>" required>
                </div>
            </div>
            <div class="form-group mt-2">
                <label for="contactEmail">Email address:</label>
                <input type="email" name="email" class="form-control" id="contactEmail" placeholder="@" value="<?php echo $email; ?>" required>
            </div>
            <div class="form-group mt-2">
                <label for="contactSubject">Subject:</label>
                <input type="text" name="subject" class="form-control" id="contactSubject" placeholder="Subject" value="<?php echo $subject; ?>" required>
            </div>
            <div class="form-group mt-2">
                <label for="contactText">Message:</label>
                <textarea class="form-control" name="message" id="contactText" rows="5" required><?php echo $message; ?></textarea>
            </div>
            <div class="form-group mt-2">
                <div class="form-check">
                    <input class="form-check-input" name="subscribe" value="Yes" type="checkbox" id="contactNewsletterCheck">
                    <label class="form-check-label" for="contactNewsletterCheck">
                        Subscribe to our newsletter to get latest news.
                    </label>
                </div>
            </div>
            <button type="submit" name="submit" class="btn btn-dark btn-lg mt-2">Send</button>
        </form>
        <?php
            // If form was submitted successfully
            if (isset($_GET['success'])) { ?>
                <p class="mt-3 fw-bold">We have received your message!</p>
            <?php
            }
        ?>
    </div>
</div>

<?php
    require_once "include/footer.php";
?>