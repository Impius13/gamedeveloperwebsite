<?php
    require_once "include/header.php";
?>

<div id="content">
    <div class="block container col-lg-7 text-center">
        <h1>Indie game dev studio</h1>
        <div class="mt-3">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ut purus lacinia, placerat nisi vitae, luctus purus. Morbi ultrices interdum dolor. Donec molestie purus magna, id accumsan magna luctus ac. Nunc libero arcu, faucibus in maximus sed, iaculis vel dolor. Integer dui eros, semper vel augue at, consectetur gravida augue. Aenean et consequat lorem, et tristique tortor. Cras auctor venenatis purus non convallis. </p>
            <p>Proin imperdiet imperdiet lectus et pulvinar. Mauris cursus faucibus accumsan. Aenean feugiat massa id porta tempor. Fusce sagittis urna quis egestas feugiat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus viverra quis ante id accumsan. Ut porttitor arcu eu ex dictum, eu dictum dolor porttitor. Proin cursus dapibus mi, at fermentum libero rutrum sit amet. Morbi vel justo consequat, pulvinar tortor sit amet, interdum est. Ut non felis a quam egestas vehicula. </p>
        </div>
    </div>
    <div class="block container col-lg-7">
        <div class="text-center">
            <h1>Our games</h1>
        </div>
        <div class="mt-4">
            <div class="clearfix">
                <div class="mb-4">
                    <img src="img/fallStuffScreen.jpg" class="games-screen col-lg-6 float-lg-end ms-4" alt="Fall Stuff Screenshot">
                    <p>Etiam libero urna, dictum in volutpat eget, aliquet vel purus. Nam convallis et ligula sit amet ultricies. Aliquam sagittis leo quis dui varius, eu condimentum est pulvinar. Aenean non metus at erat efficitur iaculis. Nullam eleifend, tellus ac ullamcorper dictum, diam ante suscipit justo, nec facilisis ligula erat ut urna. Integer pharetra mattis lorem convallis mollis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin felis mauris, hendrerit a diam ac, dictum sodales nulla. </p>
                    <p>Proin imperdiet imperdiet lectus et pulvinar. Mauris cursus faucibus accumsan. Aenean feugiat massa id porta tempor. Fusce sagittis urna quis egestas feugiat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus viverra quis ante id accumsan. Ut porttitor arcu eu ex dictum, eu dictum dolor porttitor. Proin cursus dapibus mi, at fermentum libero rutrum sit amet. Morbi vel justo consequat, pulvinar tortor sit amet, interdum est. Ut non felis a quam egestas vehicula. </p>
                    <a class="btn btn-dark btn-lg" href="games.php" role="button">Explore</a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
    require_once "include/footer.php";
?>