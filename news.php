<?php
    require_once "include/header.php";
    require_once "backend/newsGet.php";

    $newsQuery = getPosts();
    $editing = false;
    if (isset($_SESSION['employee_name']))
        $editing = true;
?>

    <div id="content">
        <?php
        // For editing news page
        if ($editing) { ?>
            <div class="block container col-lg-7 text-center">
                <a href="backend/newsEdit.php" class="btn btn-success btn-lg me-3">Add new post</a>
            </div>
        <?php } ?>

        <?php foreach($newsQuery as $q) { ?>
            <div class="block container col-lg-7 mb-4">
                <div class="row justify-content-between">
                    <h2 class="col-sm-6"><?php echo $q['title']; ?></h2>
                    <span class="col-sm-2 date"><?php $date = new DateTime($q['date']); echo $date->format('d.m.Y'); ?></span>
                </div>
                <p class="mt-3"><?php echo $q['content']; ?></p>
                <?php if ($editing) { ?>
                <div class="d-flex mt-3">
                    <a href="backend/newsEdit.php?id=<?php echo $q['id']; ?>" class="btn btn-dark btn-sm me-3">Edit</a>
                    <form action="backend/newsPost.php" method="post">
                        <input type="text" hidden name="id" value="<?php echo $q['id']; ?>">
                        <button type="submit" name="delete" class="btn btn-danger btn-sm">Delete</button>
                    </form>
                </div>
                <?php } ?>
                <hr>
            </div>
        <?php } ?>

        <div class="block container col-lg-7 mt-5">
            <h2>Subscribe to our newsletter</h2>
            <p>Never miss any important news or new releases form us!</p>
            <form action="backend/newsletter.php" method="post">
                <div class="form-group">
                    <label for="newsletterEmail">Email address:</label>
                    <div class="row">
                        <div class="col-5">
                            <input type="email" name="email" class="form-control" id="newsletterEmail" aria-describedby="emailHelp" placeholder="@" required>
                        </div>
                    </div>
                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
                <button type="submit" name="submit" class="btn btn-dark mt-2">Join!</button>
            </form>
            <?php
            // If form was submitted successfully
            if (isset($_GET['success'])) { ?>
                <p class="mt-3 fw-bold">You are now subscribed to our newsletter!</p>
                <?php
            }
            ?>
        </div>
    </div>

<?php
    require_once "include/footer.php";
?>