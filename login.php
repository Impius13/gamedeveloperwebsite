<?php
    require_once "include/header.php";
?>

<div id="content">
    <?php
        if (isset($_GET['register'])) {
    ?>
    <script defer src="js/loginValidation.js"></script>
    <div class="block container col-lg-7">
        <h2>Registration</h2>
        <?php
        if (isset($_GET['error']) && $_GET['error'] == "userregistered") {
            ?>
            <h5 class="mb-3 mt-3 warning"><u>User with this email is already registered.</u></h5>
            <?php
        }
        else if (isset($_GET['error']) && $_GET['error'] == "usernameduplicity") {
            ?>
        <h5 class="mb-3 mt-3 warning"><u>User with this name is already registered.</u></h5>
        <?php
        }
        ?>
        <form id="registerForm" action="backend/loginPost.php" method="post">
            <?php
            $name = "";
            $email = "";
            // If user left some fields empty
            if (isset($_GET['error']) && $_GET['error'] == "emptyfields") {
                $name = $_GET['name'];
                $email = $_GET['email'];
            }
            ?>
            <div class="form-group mt-2">
                <label for="registerName">Username:</label>
                <input type="text" name="name" class="form-control" id="registerName" value="<?php echo $name; ?>" required>
            </div>
            <div class="form-group mt-2">
                <label for="registerEmail">Email:</label>
                <input type="email" name="email" class="form-control" id="registerEmail" placeholder="@" value="<?php echo $email; ?>" required>
            </div>
            <div class="form-group mt-2">
                <label for="registerPassword">Password:</label>
                <input type="password" name="password" class="form-control" id="registerPassword" required>
                <div class="invalid-feedback">
                    Password must be between 8 and 20 characters
                </div>
            </div>
            <div class="form-group mt-2">
                <label for="registerPasswordCheck">Confirm password:</label>
                <input type="password" name="passwordCheck" class="form-control" id="registerPasswordCheck" required>
                <div class="invalid-feedback">
                    Passwords do not match
                </div>
            </div>
            <button type="submit" name="register" class="btn btn-dark mt-4">Register</button>
        </form>
    </div>
    <?php
        } else {
    ?>
    <div class="block container col-lg-7">
        <?php
            if (isset($_GET['success'])) {
        ?>
            <h4 class="mb-3">Registration successful!</h4>
        <?php
            }
        ?>
        <h2>Login</h2>
        <?php
        if (isset($_GET['error']) && $_GET['error'] == "wrongcredentials") {
            ?>
            <h5 class="mb-3 mt-3 warning"><u>Wrong email or password.</u></h5>
            <?php
        }
        ?>
        <form action="backend/loginPost.php" method="post">
            <?php
            $email = "";
            // If user left some fields empty
            if (isset($_GET['error'])) {
                $email = $_GET['email'];
            }
            ?>
            <div class="form-group mt-2">
                <label for="loginEmail">Email:</label>
                <input type="email" name="email" class="form-control" id="loginEmail" placeholder="@" value="<?php echo $email; ?>" required>
            </div>
            <div class="form-group mt-2">
                <label for="loginPassword">Password:</label>
                <input type="password" name="password" class="form-control" id="loginPassword" required>
            </div>
            <button type="submit" name="login" class="btn btn-dark mt-4">Login</button>
        </form>
    </div>
    <div class="block container col-lg-7 mt-5">
        <h3>Not registered yet?</h3>
        <p>Register here:</p>
        <a class="btn btn-dark btn-lg mt-2" href="login.php?register=1" role="button">Register</a>
    </div>
    <?php
        }
    ?>
</div>

<?php
    require_once "include/footer.php";
?>