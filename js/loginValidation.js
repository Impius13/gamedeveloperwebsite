const password = document.getElementById('registerPassword');
const passwordCheck = document.getElementById('registerPasswordCheck');
const form = document.getElementById('registerForm');

form.addEventListener('submit', (e) => {
    let submit = true;
    password.classList.remove("is-invalid");
    passwordCheck.classList.remove("is-invalid");

    if (password.value.length <= 8 || password.value.length >= 20) {
        password.classList.add("is-invalid");
        submit = false;
    }

    if (password.value !== passwordCheck.value) {
        passwordCheck.classList.add("is-invalid");
        submit = false;
    }

    if (!submit) {
        e.preventDefault();
    }
})