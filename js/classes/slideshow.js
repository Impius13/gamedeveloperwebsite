export default class Slideshow {
    constructor(images, sliderElem) {
        this.images = images;
        this.sliderElem = sliderElem;

        this.i = 0;
    }

    nextSlide() {
        // Set next image
        this.sliderElem.src = this.images[this.i];

        if (this.i == this.images.length - 1) {
            this.i = 0;
        } else {
            this.i++;
        }
    }
}