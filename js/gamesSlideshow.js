import Slideshow from "./classes/slideshow.js";

let sliderElem = document.getElementById("slider");
let images = [];
images[0] = "img/fallStuff1.jpg";
images[1] = "img/fallStuff2.jpg";
images[2] = "img/fallStuffMenu.jpg";

let slider = new Slideshow(images, sliderElem);
const timeout = 3000;

function changeSlide() {
    slider.nextSlide();
}

changeSlide();
let intervalId = setInterval(changeSlide, timeout);

sliderElem.addEventListener('mouseenter', () => {
    clearInterval(intervalId);
})

sliderElem.addEventListener('mouseleave', () => {
    intervalId = setInterval(changeSlide, timeout);
})

sliderElem.addEventListener('click', () => {
    changeSlide();
})