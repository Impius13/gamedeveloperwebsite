<?php
    session_start();
    $_SESSION['currentPage'] = $_SERVER['PHP_SELF'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <link href="style.css?version=1" rel="stylesheet" type="text/css">
</head>

<body>
<header>
    <div class="row pt-2 justify-content-center">
        <div class="col">
            <img src="img/logo.png" class="img-fluid" alt="Logo">
            <h2>Impius Industries</h2>
        </div>
    </div>
    <nav id="nav-menu" class="navbar navbar-expand navbar-dark flex-column">
        <div class="container-fluid">
            <div class="navbar-collapse justify-content-center">
                <ul class="navbar-nav navbar-center">
                    <li class="nav-item">
                        <a class="nav-link <?php echo (strcmp($_SERVER['PHP_SELF'], "/DeveloperStranka/index.php") ? "" : "active");?>" href="index.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php echo (strcmp($_SERVER['PHP_SELF'], "/DeveloperStranka/games.php") ? "" : "active");?>" href="games.php">Games</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php echo (strcmp($_SERVER['PHP_SELF'], "/DeveloperStranka/about.php") ? "" : "active");?>" href="about.php">About us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php echo (strcmp($_SERVER['PHP_SELF'], "/DeveloperStranka/news.php") ? "" : "active");?>" href="news.php">News</a>
                    </li>
                </ul>
                <ul class="navbar-nav flex-row ml-md-auto">
                <?php
                    if (!isset($_SESSION['username'])) {
                ?>
                    <li class="nav-item">
                        <a class="nav-link" href="login.php">Login</a>
                    </li>
                <?php
                    }
                ?>
                <?php
                    if (isset($_SESSION['employee_name'])) {
                ?>
                <li class="editing nav-item">
                    <a class="nav-link" href="backend/usersEdit.php">Edit users</a>
                </li>
                    <?php
                        if ($_SESSION['username'] == "admin") {
                    ?>
                    <li class="editing nav-item">
                        <a class="nav-link" href="backend/employeesEdit.php">Edit employees</a>
                    </li>
                    <?php
                        }
                    ?>
                <?php
                    }
                ?>
                </ul>
            </div>
        </div>
    </nav>
    <?php
    if (isset($_SESSION['username'])) {
        ?>
        <div class="logged">
            <p>Logged as <?php echo $_SESSION['username']?> | <a href="backend/logout.php">LOGOUT</a></p>
            <?php
                if (isset($_SESSION['employee_name'])) {
            ?>
            <p>Editing privileges granted for employee <?php echo $_SESSION['employee_name']." ".$_SESSION['employee_surname'] ?></p>
            <?php
                }
            ?>
        </div>
        <?php
    }
    ?>
</header>