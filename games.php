<?php
    require_once "include/header.php";
?>

<script type="module" src="js/gamesSlideshow.js"></script>
<div id="content">
    <div class="block container col-lg-7">
        <h1>Fall Stuff</h1>
        <div class="block row justify-content-between">
            <div class="col-md-2">
                <img id="slider" class="" width="200" height="400">
            </div>
            <div class="col-md-8">
                <p class="fw-bold">Fall Stuff is a 2D mobile platforming game available on Android.</p>
                <ul>
                    <li>
                        Dodge falling stuff and get as high as you can!
                    </li>
                    <li>
                        Different power ups to help you along the way. Slow time, double jump or super speed.
                    </li>
                    <li>
                        Special events with thematic falling stuff.
                    </li>
                    <li>
                        Is there something on top?
                    </li>
                    <li>
                        Hats and skins coming soon!
                    </li>
                </ul>
                <p class="mt-5 fw-bold">Download on Google Play:</p>
                <a class="btn btn-dark btn-lg" href="games.php" role="button">Google Play</a>
            </div>
        </div>
    </div>
    <div class="block container col-lg-7 mt-5">
        <h1>Cloak & Daggers</h1>
        <div class="row">
            <div class="mt-3">
                <p class="fw-bold">Ancient, forgotten project. We don't really talk about that here...</p>
                <ul>
                    <li>
                        Text adventure RPG made in python.
                    </li>
                    <li>
                        Story about betrayal.
                    </li>
                    <li>
                        Play as a former mercenary that wants a peaceful life.
                    </li>
                    <li>
                        Complete quests, level up and uncover the truth.
                    </li>
                </ul>
                <p>Download unavailable. If you really want to play this old, buggy mess, contact us. But beware!</p>
            </div>
        </div>
    </div>
</div>

<?php
    require_once "include/footer.php";
?>