<?php
session_start();

include "model/News.php";

$news = new News();

$headerBack = "Location: ../news.php?";
$headerUpdate = "Location: newsEdit.php?";

if (isset($_POST['submit'])) {
    $title = $_POST['title'];
    $content = $_POST['content'];
    $date = date('Y-m-d');
    $employee_id = $_SESSION['employee_id'];

    $headerUpdate = "Location: newsEdit.php?";

    if (empty($title) || empty($content)) {
        header($headerUpdate."title=".$title."&content=".$content);
    } else {
        $response = $news->addPost($title, $content, $date, $employee_id);
        header($headerBack.$response);
    }
}

if (isset($_POST['update'])) {
    $id = $_POST['id'];
    $title = $_POST['title'];
    $content = $_POST['content'];
    $date = date('Y-m-d');
    $employee_id = $_SESSION['employee_id'];

    if (empty($title) || empty($content)) {
        header($headerUpdate."id=".$id."&error=emptyfields");
    } else {
        $response = $news->updatePost($id, $title, $content, $date, $employee_id);
        header($headerBack.$response);
    }
}

if (isset($_POST['delete'])) {
    $id = $_POST['id'];

    $response = $news->deleteEntry($id);
    header($headerBack.$response);
}