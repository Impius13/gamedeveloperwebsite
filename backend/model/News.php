<?php

include_once "TableModel.php";

class News extends TableModel
{
    function __construct() {
        parent::__construct("news_posts");
    }

    public function addPost($title, $content, $date, $employeeId) {
        if ($this->db->checkDuplicity($this->tableName, "title", $title, "s")) {
            return "error=titleduplicity";
        } else if ($this->db->getQuery("SELECT id FROM employees WHERE id = ".$employeeId) == false) {
            return "error=employeenotexists";
        } else {
            $sql = "INSERT INTO ".$this->tableName." (title, content, date, employee_id) VALUES (?, ?, ?, ?)";
            $stmt = $this->db->prepareStatement($sql);
            if (!isset($stmt)) {
                // Invalid query
                return "error=sqlerror";
            } else {
                mysqli_stmt_bind_param($stmt, "sssi", $title, $content, $date, $employeeId);
                $this->db->executeAndStore($stmt);

                return "success=1";
            }
        }
    }

    public function updatePost($id, $title, $content, $date, $employeeId) {
        if (!$this->db->checkDuplicity($this->tableName, "id", $id, "i")) {
            return "error=postnotexists";
        } else if ($this->db->getQuery("SELECT id FROM employees WHERE id = ".$employeeId) == false) {
            return "error=employeenotexists";
        } else {
            $sql = "UPDATE ".$this->tableName." SET title = ?, content = ?, date = ?, employee_id = ? WHERE id = $id";
            $stmt = $this->db->prepareStatement($sql);
            if (!isset($stmt)) {
                // Invalid query
                return "error=sqlerror";
            } else {
                mysqli_stmt_bind_param($stmt, "sssi", $title, $content, $date, $employeeId);
                $this->db->executeAndStore($stmt);

                return "success=1";
            }
        }
    }
}