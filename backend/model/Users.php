<?php

include_once "TableModel.php";

class Users extends TableModel
{
    function __construct() {
        parent::__construct("users");
    }

    public function addUserNewsletter($email, $newsletter) {
        if ($this->db->checkDuplicity($this->tableName, "email", $email, "s")) {
            return "error=userduplicity";
        } else {
            $sql = "INSERT INTO ".$this->tableName." (email, newsletter) VALUES (?, ?)";
            $stmt = $this->db->prepareStatement($sql);
            if (!isset($stmt)) {
                // Invalid query
                return "error=sqlerror";
            } else {
                mysqli_stmt_bind_param($stmt, "si", $email, $newsletter);
                $this->db->executeAndStore($stmt);

                return "success=1";
            }
        }
    }

    public function addUser($email, $newsletter, $username, $password) {
        if ($this->db->checkDuplicity($this->tableName, "email", $email, "s")) {
            // Check if the user is already registered or if he is only subscribed
            $query = $this->getByColumn("email", "'".$email."'");
            $usernameCheck = null;
            $userId = null;
            if (isset($query)) {
                foreach ($query as $q) {
                    $usernameCheck = $q["username"];
                    $userId = $q["id"];
                }
            }
            if (isset($usernameCheck)) {
                return "error=userregistered";
            } else if ($this->db->checkDuplicity($this->tableName, "username", $username, "s")) {
                // TODO: Zle kontrolovanie duplicity
                return "error=usernameduplicity";
            } else {
                // Update user data
                return $this->updateUser($userId, $email, $newsletter, $username, $password);
            }
        } else {
            // Add new user
            $sql = "INSERT INTO ".$this->tableName." (email, newsletter, username, password) VALUES (?, ?, ?, ?)";
            $stmt = $this->db->prepareStatement($sql);
            if (!isset($stmt)) {
                // Invalid query
                return "error=sqlerror";
            } else {
                mysqli_stmt_bind_param($stmt, "siss", $email, $newsletter, $username, $password);
                $this->db->executeAndStore($stmt);

                return "success=1";
            }
        }
    }

    public function updateUser($id, $email, $newsletter, $username, $password) {
        if (!$this->db->checkDuplicity($this->tableName, "id", $id, "i")) {
            return "error=usernotexists";
        } else {
            $sql = "UPDATE ".$this->tableName." SET email = ?, newsletter = ?, username = ?, password = ? WHERE id = $id";
            $stmt = $this->db->prepareStatement($sql);
            if (!isset($stmt)) {
                // Invalid query
                return "error=sqlerror";
            } else {
                mysqli_stmt_bind_param($stmt, "siss", $email, $newsletter, $username, $password);
                $this->db->executeAndStore($stmt);

                return "success=1";
            }
        }
    }
}