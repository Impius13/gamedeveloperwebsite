<?php

// Connects to database and handles queries
class Database {
    private $host;
    private $user;
    private $password;
    private $dbName;

    private $conn;

    function __construct() {
        $this->host = "localhost";
        $this->user = "root";
        $this->password = "";
        $this->dbName = "impius_industries";

        $this->conn = mysqli_connect($this->host, $this->user, $this->password, $this->dbName);
        if (!$this->conn) {
            die("Database connection failed!");
        }
    }

    function __destruct() {
        mysqli_close($this->conn);
    }

    public function getQuery($sql) {
        return mysqli_query($this->conn, $sql);
    }

    public function checkDuplicity($tableName, $column, $data, $dataType) {
        $sql = "SELECT ".$column." FROM ".$tableName." WHERE ".$column. " = ?";
        $stmt = $this->prepareStatement($sql);
        if (!isset($stmt)) {
            return true;
        } else {
            mysqli_stmt_bind_param($stmt, $dataType, $data);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_store_result($stmt);

            $rowCount = mysqli_stmt_num_rows($stmt);
            if ($rowCount > 0) {
                return true;
            }
        }
        return false;
    }

    public function deleteEntry($tableName, $id) {
        $sql = "DELETE FROM ".$tableName." WHERE id = ?";
        $stmt = $this->prepareStatement($sql);
        if (!isset($stmt)) {
            return "error=sqlerror";
        } else {
            mysqli_stmt_bind_param($stmt, "i", $id);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_store_result($stmt);

            return "success=1";
        }
    }

    public function prepareStatement($sql) {
        $stmt = mysqli_stmt_init($this->conn);
        if (mysqli_stmt_prepare($stmt, $sql)) {
            return $stmt;
        }
        return null;
    }

    public function executeAndStore($stmt) {
        mysqli_stmt_execute($stmt);
        mysqli_stmt_store_result($stmt);
    }
}
