<?php

include_once "TableModel.php";

class UserComments extends TableModel
{
    function __construct() {
        parent::__construct("user_comments");
    }

    public function addComment($comment, $date, $user_id) {
        if ($this->db->getQuery("SELECT id FROM users WHERE id = ".$user_id) == false) {
            return "error=usernotexists";
        } else {
            $sql = "INSERT INTO " . $this->tableName . " (comment, date, user_id) VALUES (?, ?, ?)";
            $stmt = $this->db->prepareStatement($sql);
            if (!isset($stmt)) {
                // Invalid query
                return "error=sqlerror";
            } else {
                mysqli_stmt_bind_param($stmt, "ssi", $comment, $date, $user_id);
                $this->db->executeAndStore($stmt);

                return "success=1";
            }
        }
    }

    public function updateComment($id, $comment, $date, $user_id) {
        if (!$this->db->checkDuplicity($this->tableName, "id", $id, "i")) {
            return "error=commentnotexists";
        } else if ($this->db->getQuery("SELECT id FROM users WHERE id = ".$user_id) == false) {
            return "error=usernotexists";
        } else {
            $sql = "UPDATE ".$this->tableName." SET comment = ?, date = ? WHERE id = $id";
            $stmt = $this->db->prepareStatement($sql);
            if (!isset($stmt)) {
                // Invalid query
                return "error=sqlerror";
            } else {
                mysqli_stmt_bind_param($stmt, "ss", $comment, $date);
                $this->db->executeAndStore($stmt);

                return "success=1";
            }
        }
    }
}