<?php

include_once "TableModel.php";

class Employees extends TableModel
{
    function __construct() {
        parent::__construct("employees");
    }

    public function addEmployee($name, $surname, $user_id) {
        if ($this->db->checkDuplicity($this->tableName, "user_id", $user_id, "i")) {
            return "error=employeeregistered";
        } else if ($this->db->getQuery("SELECT id FROM users WHERE id = ".$user_id) == false) {
            return "error=usernotexists";
        } else {
            $sql = "INSERT INTO ".$this->tableName." (name, surname, user_id) VALUES (?, ?, ?)";
            $stmt = $this->db->prepareStatement($sql);
            if (!isset($stmt)) {
                // Invalid query
                return "error=sqlerror";
            } else {
                mysqli_stmt_bind_param($stmt, "ssi", $name, $surname, $user_id);
                $this->db->executeAndStore($stmt);

                return "success=1";
            }
        }
    }

    public function updateEmployee($id, $name, $surname, $user_id) {
        if (!$this->db->checkDuplicity($this->tableName, "id", $id, "i")) {
            return "error=employeenotexists";
        } else if ($this->db->getQuery("SELECT id FROM users WHERE id = ".$user_id) == false) {
            return "error=usernotexists";
        } else {
            $sql = "UPDATE ".$this->tableName." SET name = ?, surname = ?, user_id = ? WHERE id = $id";
            $stmt = $this->db->prepareStatement($sql);
            if (!isset($stmt)) {
                // Invalid query
                return "error=sqlerror";
            } else {
                mysqli_stmt_bind_param($stmt, "ssi", $name, $surname, $user_id);
                $this->db->executeAndStore($stmt);

                return "success=1";
            }
        }
    }
}