<?php

include_once "Database.php";

class TableModel
{
    protected $db;
    protected $tableName;

    function __construct($tableName) {
        $this->db = new Database();
        $this->tableName = $tableName;
    }

    public function getAll() {
        return $this->db->getQuery("SELECT * FROM ".$this->tableName);
    }

    public function getAllDesc() {
        return $this->db->getQuery("SELECT * FROM ".$this->tableName." ORDER BY id DESC");
    }

    public function getById($id) {
        return $this->db->getQuery("SELECT * FROM ".$this->tableName." WHERE id = ".$id);
    }

    public function getByColumn($column, $value) {
        return $this->db->getQuery("SELECT * FROM ".$this->tableName." WHERE ".$column." = ".$value);
    }

    public function deleteEntry($id) {
        $this->db->deleteEntry($this->tableName, $id);
    }
}