<?php
    require_once "../include/headerBasic.php";
    include "model/Employees.php";
    $employees = new Employees();

    $query = $employees->getAll();
?>

<body>
    <div class="block container col-lg-7 mb-4">
        <h4><a class="edit-back" href="../index.php"><-- Back</a></h4>
        <h2>Employees database</h2>
        <div class="block container col-lg-7 text-center">
            <a href="employeeUpdate.php" class="btn btn-success me-3">Add new employee</a>
        </div>
        <?php
        // Get all employees
        if (isset($query)) {
            foreach($query as $q) {
                ?>
                <div class="block container mb-4">
                    <p><b>ID: </b><?php echo $q['id'] ?></p>
                    <p><b>Name: </b><?php echo $q['name'] ?></p>
                    <p><b>Surname: </b><?php echo $q['surname'] ?></p>
                    <p><b>User ID: </b><?php echo $q['user_id'] ?></p>
                    <div class="d-flex mt-3">
                        <a href="employeeUpdate.php?id=<?php echo $q['id']; ?>" class="btn btn-dark btn-sm me-3">Edit</a>
                        <form action="employeePost.php" method="post">
                            <input type="text" hidden name="id" value="<?php echo $q['id']; ?>">
                            <button type="submit" name="delete" class="btn btn-danger btn-sm">Delete</button>
                        </form>
                    </div>
                </div>
            <?php } } ?>
    </div>
</body>