<?php
    if (isset($_POST['submit'])) {
        $name = trim($_POST['name']);
        $surname = trim($_POST['surname']);
        $email = trim($_POST['email']);
        $subject = trim($_POST['subject']);
        $message = trim($_POST['message']);

        $receiver = "info@impindustries.com";
        $header = "From: " . $email;

        $pageHeader = "Location: ../about.php?";

        if (empty($name) || empty($surname) || empty($email) || empty($subject)
            || empty($message)) {
            // If some fields are empty, send them back and exit
            header($pageHeader."error=emptyfields&name=".$name.
            "&surname=".$surname."&email=".$email."&subject=".$subject."&message=".$message);
            exit();
        } else {
            // Send email
            mail($receiver, $subject, $message, $header);

            if (isset($_POST['subscribe']) && $_POST['subscribe'] == 'Yes') {
                // Connect to database and add user data
                require "model/Users.php";

                $users = new Users();
                $response = $users->addUser($email, 1);
                header($pageHeader.$response);
                exit();
            }

            // Redirect back to about page
            header($pageHeader."success=1");
        }
    }