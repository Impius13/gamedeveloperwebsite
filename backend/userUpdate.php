<?php
    require_once "../include/headerBasic.php";

    include "model/Users.php";
    $users = new Users();

    $query = null;
    if (isset($_GET['id'])) {
        $id = $_GET['id'];
        $query = $users->getById($id);
    }
?>

<body>
<div class="block container col-lg-7">
    <?php if (isset($query)) {
        foreach ($query as $q) { ?>
            <h2>Edit user</h2>
            <form action="usersPost.php" method="post">
                <input type="text" hidden name="id" value="<?php echo $q['id']; ?>">
                <div class="form-group">
                    <label for="editEmail">Email:</label>
                    <input type="email" name="email" class="form-control" id="editEmail" placeholder="@" value="<?php echo $q['email']; ?>">
                </div>
                <div class="form-group">
                    <label for="editUsername">Username:</label>
                    <input type="text" name="username" class="form-control" id="editUsername" value="<?php echo $q['username']; ?>">
                </div>
                <div class="form-group">
                    <label for="editPassword">Password:</label>
                    <input type="text" name="password" class="form-control" id="editPassword" value="<?php echo $q['password']; ?>">
                </div>
                <div class="form-group mt-2">
                    <div class="form-check">
                        <input class="form-check-input" name="newsletter" value="Yes" type="checkbox" id="editNewsletter">
                        <label class="form-check-label" for="editNewsletter">
                            Newsletter
                        </label>
                    </div>
                </div>
                <button type="submit" name="update" class="btn btn-dark btn-lg mt-3">Update</button>
            </form>
        <?php } }
    else { ?>
        <div class="block container col-lg-7">
            <h2>No user found</h2>
        </div>
    <?php } ?>
</div>
</body>
</html>