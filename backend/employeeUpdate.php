<?php
    require_once "../include/headerBasic.php";
    include "model/Employees.php";
    $empoyees = new Employees();

    $query = null;
    if (isset($_GET['id'])) {
        $id = $_GET['id'];
        $query = $empoyees->getById($id);
    }
?>

<body>
<div class="block container col-lg-7">
    <?php if (isset($query)) {
        foreach ($query as $q) { ?>
            <h2>Edit employee</h2>
            <form action="employeePost.php" method="post">
                <input type="text" hidden name="id" value="<?php echo $q['id']; ?>">
                <div class="form-group">
                    <label for="editName">Name:</label>
                    <input type="text" name="name" class="form-control" id="editName" value="<?php echo $q['name']; ?>">
                </div>
                <div class="form-group">
                    <label for="editSurname">Surname:</label>
                    <input type="text" name="surname" class="form-control" id="editSurname" value="<?php echo $q['surname']; ?>">
                </div>
                <div class="form-group">
                    <label for="editUserId">User ID:</label>
                    <input type="number" name="user_id" class="form-control" id="editUserId" value="<?php echo $q['user_id']; ?>">
                </div>
                <button type="submit" name="update" class="btn btn-dark btn-lg mt-3">Update</button>
            </form>
        <?php } }
    else { ?>
        <h2>Add employee</h2>
        <form action="employeePost.php" method="post">
            <div class="form-group">
                <label for="employeeName">Name:</label>
                <input type="text" name="name" class="form-control" id="employeeName" value="">
            </div>
            <div class="form-group">
                <label for="employeeSurname">Surname:</label>
                <input type="text" name="surname" class="form-control" id="employeeSurname" value="">
            </div>
            <div class="form-group">
                <label for="employeeUserId">User ID:</label>
                <input type="number" name="user_id" class="form-control" id="employeeUserId" value="">
            </div>
            <button type="submit" name="submit" class="btn btn-dark btn-lg mt-3">Submit</button>
        </form>
    <?php } ?>
</div>
</body>
</html>