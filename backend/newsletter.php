<?php
    if (isset($_POST['submit'])) {
        $email = $_POST['email'];

        $header = "Location: ../news.php?";
        if (empty($email)) {
            header($header."error=emptyfields");
            exit();
        } else {
            require "model/Users.php";

            $users = new Users();
            $response = $users->addUserNewsletter($email, 1);
            header($header.$response);
            exit();
        }
    }
