<?php
    require_once "../include/headerBasic.php";

    include "model/News.php";
    $news = new News();

    $query = null;
    if (isset($_GET['id'])) {
        $id = $_GET['id'];
        $query = $news->getById($id);
    }
?>

<body>
    <div class="block container col-lg-7">
        <?php if (isset($query)) {
            foreach ($query as $q) { ?>
        <h2>Edit post</h2>
        <form action="newsPost.php" method="post">
            <input type="text" hidden name="id" value="<?php echo $q['id']; ?>">
            <div class="form-group">
                <label for="editTitle">Post title:</label>
                <input type="text" name="title" class="form-control" id="editTitle" placeholder="Title" value="<?php echo $q['title']; ?>">
            </div>
            <div class="form-group mt-2">
                <label for="editContent">Content:</label>
                <textarea class="form-control" name="content" id="editContent" rows="5"><?php echo $q['content']; ?></textarea>
            </div>
            <button type="submit" name="update" class="btn btn-dark btn-lg mt-3">Update</button>
        </form>
        <?php } }
            else { ?>
        <div class="block container col-lg-7">
            <h2>Create new post</h2>
            <form action="newsPost.php" method="post">
                <div class="form-group">
                    <label for="postTitle">Post title:</label>
                    <input type="text" name="title" class="form-control" id="postTitle" placeholder="Title">
                </div>
                <div class="form-group mt-2">
                    <label for="postContent">Content:</label>
                    <textarea class="form-control" name="content" id="postContent" rows="5"></textarea>
                </div>
                <button type="submit" name="submit" class="btn btn-dark btn-lg mt-3">Submit</button>
            </form>
        </div>
        <?php } ?>
    </div>
</body>
</html>