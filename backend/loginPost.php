<?php

include "model/Users.php";
include "model/Employees.php";

session_start();

$headerIndex = "Location: ../index.php";
$headerRegister = "Location: ../login.php?register=1&";
$headerLogin = "Location: ../login.php?";

if (isset($_POST['login'])) {
    $email = trim($_POST['email']);
    $password = trim($_POST['password']);

    if (empty($email)) {
        header($headerLogin."email=".$email."&error=emptyfields");
    } else {
        $users = new Users();

        $query = $users->getByColumn("email", "'$email'");
        $userId = null;
        $userPassword = null;
        $userName = null;
        foreach ($query as $q) {
            $userId = $q['id'];
            $userPassword = $q['password'];
            $userName = $q['username'];
        }
        if (isset($userPassword) && password_verify($password, $userPassword)) {
            $_SESSION['username'] = $userName;

            // Check if user is an employee
            $employees = new Employees();

            $query = $employees->getByColumn("user_id", $userId);
            if ($query != false) {
                $employeeName = null;
                $employeeSurname = null;
                $employeeId = null;

                foreach ($query as $q) {
                    $employeeName = $q['name'];
                    $employeeSurname = $q['surname'];
                    $employeeId = $q['id'];
                }

                $_SESSION['employee_name'] = $employeeName;
                $_SESSION['employee_surname'] = $employeeSurname;
                $_SESSION['employee_id'] = $employeeId;
            }

            header($headerIndex);
        } else {
            header($headerLogin."error=wrongcredentials&email=".$email);
        }
    }
}

if (isset($_POST['register'])) {
    $username = $_POST['name'];
    $email = trim($_POST['email']);
    $password = trim($_POST['password']);

    if (empty($username) || empty($email) || empty($password)) {
        header($headerRegister."name=".$username."&email=".$email."&error=emptyfields");
    } else {
        // Salt hash password
        $hashedPassword = password_hash($password, PASSWORD_DEFAULT);
        $users = new Users();

        $response = $users->addUser($email, 0, $username, $hashedPassword);

        if (strcmp($response, "success=1") == 0)
            header($headerLogin.$response);
        else
            header($headerRegister.$response);
    }
}