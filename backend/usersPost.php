<?php
include "model/Users.php";

$headerUpdate = "Location: userUpdate.php?";
$headerBack = "Location: usersEdit.php?";

if (isset($_POST['update'])) {
    $id = $_POST['id'];
    $email = $_POST['email'];
    $username = $_POST['username'];
    $password = $_POST['password'];

    if (empty($email) || empty($username) || empty($password)) {
        header($headerUpdate."id=".$id."&error=emptyfields");
    } else {
        $users = new Users();

        $newsletter = 0;
        if (isset($_POST['newsletter']) && $_POST['newsletter'] == 'Yes') {
            $newsletter = 1;
        }

        // Hash password
        $hashedPassword = password_hash($password, PASSWORD_DEFAULT);

        $response = $users->updateUser($id, $email, $newsletter, $username, $hashedPassword);
        header($headerBack.$response);
    }
}

if (isset($_POST['delete'])) {
    $id = $_POST['id'];

    $users = new Users();

    $response = $users->deleteEntry($id);
    header($headerBack.$response);
}