<?php
    require_once "../include/headerBasic.php";

    include "model/Users.php";
    $users = new Users();

    $query = $users->getAll();
?>

<body>
    <div class="block container col-lg-7 mb-4">
        <h4><a class="edit-back" href="../index.php"><-- Back</a></h4>
        <h2>Users database</h2>
        <?php
            // Get all users
            if (isset($query)) {
                foreach($query as $q) {
        ?>
        <div class="block container mb-4">
            <p><b>ID: </b><?php echo $q['id'] ?></p>
            <p><b>E-mail: </b><?php echo $q['email'] ?></p>
            <p><b>Newsletter: </b><?php echo $q['newsletter'] ?></p>
            <p><b>Username: </b><?php echo $q['username'] ?></p>
            <p><b>Password: </b><?php echo $q['password'] ?></p>
            <div class="d-flex mt-3">
                <a href="userUpdate.php?id=<?php echo $q['id']; ?>" class="btn btn-dark btn-sm me-3">Edit</a>
                <form action="usersPost.php" method="post">
                    <input type="text" hidden name="id" value="<?php echo $q['id']; ?>">
                    <button type="submit" name="delete" class="btn btn-danger btn-sm">Delete</button>
                </form>
            </div>
        </div>
        <?php } } ?>
    </div>
</body>