<?php
include "model/Employees.php";

$headerUpdate = "Location: employeeUpdate.php?";
$headerBack = "Location: employeesEdit.php?";

if (isset($_POST['submit'])) {
    $name = $_POST['name'];
    $surname = $_POST['surname'];
    $user_id = $_POST['user_id'];

    if (empty($name) || empty($surname) || empty($user_id)) {
        header($headerUpdate."&error=emptyfields");
    } else {
        $employees = new Employees();

        $response = $employees->addEmployee($name, $surname, $user_id);
        header($headerBack.$response);
    }
}

if (isset($_POST['update'])) {
    $id = $_POST['id'];
    $name = $_POST['name'];
    $surname = $_POST['surname'];
    $user_id = $_POST['user_id'];

    if (empty($name) || empty($surname) || empty($user_id)) {
        header($headerUpdate."&error=emptyfields");
    } else {
        $employees = new Employees();

        $response = $employees->updateEmployee($id, $name, $surname, $user_id);
        header($headerBack.$response);
    }
}

if (isset($_POST['delete'])) {
    $id = $_POST['id'];

    $employees = new Employees();

    $response = $employees->deleteEntry($id);
    header($headerBack.$response);
}