<?php
session_start();
$previousPage = $_SESSION['currentPage'];
session_destroy();
header("Location: ".$previousPage);